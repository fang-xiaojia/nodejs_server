// sql语句
var sqlMap = {
	user: {
		// 查询首页图片数据 where name like "%"?"%"'
		select: 'select * from images where type=?',
		// 查询闲置商品数据
		selectXz: 'select r.r_xz,r.time,u.userName,u.userImg,u.wechatNum,u.phoneNum,xz.price,xz.detail,xz.img1,xz.img2,xz.img3,xz.no,xz.like_value,xz.watch_value from zc_xz as xz join zc_releace r join user as u where xz.No = r.r_xz and r.openid = u.openid GROUP BY r.time  ORDER BY r.time DESC LIMIT ?,?',
		//user表新用户添加
		NewInsertTouser: 'INSERT INTO user(openid,userid,userName,userImg,userType,phoneNum,wechatNum,photo) VALUES(?,?,?,?,?,?,?,?)',
		//user表 通过微信唯一标识码openid 查询用户表内是否有该用户
		selectUser: 'SELECT * from user WHERE openid=?',
		//跑腿添加地址
		Ptinsert_address:'INSERT into zc_address(openid,name,phoneNum,place,address,areavalue,default_type) VALUES(?,?,?,?,?,?,?)',
		//跑腿用户地址查询
		Ptselect_address:'SELECT *  from zc_address where openid=? ORDER BY default_type DESC',
		//跑腿编辑地址-修改
		Ptupdate_address:'UPDATE zc_address SET name=?,phoneNum=?,place=?,address=?,areavalue=?,default_type=? WHERE NO=? and openid=?',
		//跑腿编辑地址--删除
		PtDelete_address:'DELETE FROM zc_address WHERE NO=? and openid=?',
		//跑腿修改默认值-添加过程协调修改
		Ptupdate_defalut:'UPDATE zc_address SET default_type=0 WHERE default_type=1 and openid=?',
		//跑腿帮我取(买)-下单
		Ptinsert_order:'INSERT into zc_pt(No,address_No,time,by_Name,price,`range`,phoneNum,detail,img) VALUES(?,?,?,?,?,?,?,?,?)',
		//跑腿帮我取(买)-发布表添加
		Ptinsert_releace_order:'INSERT into zc_releace(r_id,openid,r_pt,time) VALUES(?,?,?,?)',
		//发布-失物招领
		Fbinsert_xw_add:'INSERT into zc_xw(No,name,phone,title,detail,type,img) VALUES(?,?,?,?,?,?,?)',
		//发布-失物招领-添加发布记录
		Fbinsert_releace_xw_add:'INSERT into zc_releace(r_id,openid,r_xw,time) VALUES(?,?,?,?)',
		//发布-二手闲置
		Fbinsert_xz_add:'INSERT into zc_xz(No,price,like_value,watch_value,phoneNum,wechatNum,detail,type,img) VALUES(?,?,?,?,?,?,?,?,?)',
		//发布-二手闲置-添加发布记录
		Fbinsert_releace_xz_add:'INSERT into zc_releace(r_id,openid,r_xz,time) VALUES(?,?,?,?)',
		//发布-兼职发布
		Fbinsert_jz_add:'INSERT into zc_jz(No,title_name,address,time,work_date,price,work_detail,work_require,company,phoneNum,count,type) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)',
		//发布-兼职发布-添加发布记录
		Fbinsert_releace_jz_add:'INSERT into zc_releace(r_id,openid,r_jz,time) VALUES(?,?,?,?)',
	}
}

module.exports = sqlMap;
