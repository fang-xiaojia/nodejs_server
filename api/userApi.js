const express = require('express');
const router = express.Router();
const DBHelper = require('../utils/DBHelper');
const sql = require('../sqlMap');
var jsencrypt1 = require("../Encorypt/jsencrypt.js")
var jsencrypt = new jsencrypt1();
var md5 = require('md5-node')
const axios = require('axios')
const {
	json
} = require('express');
//从服务端到客户端的加密
function getRSAcode(str) {
	var publiukey = `-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDQydt3j1JANCpl0kndkQJwdoF0
jCePkewC9JIpfZOEOLrLTvQNmlQhAFY9KetrsZMg4hv2SUYLY5bwksNE38XLi3m8
pF1XJvodRDrqotNf+4qStKMl3J4X8CM094z0dKWBxMaCwLMiZtNyGqpbdYY6Gqit
x5R6d3Xik6bTBHWOWwIDAQAB
-----END PUBLIC KEY-----`;
	var pubblicData1 = jsencrypt.setLongEncrypt(publiukey, str);
	return pubblicData1;
}

function getRSALongcode(str) {
	var publiukey = `-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDQydt3j1JANCpl0kndkQJwdoF0
jCePkewC9JIpfZOEOLrLTvQNmlQhAFY9KetrsZMg4hv2SUYLY5bwksNE38XLi3m8
pF1XJvodRDrqotNf+4qStKMl3J4X8CM094z0dKWBxMaCwLMiZtNyGqpbdYY6Gqit
x5R6d3Xik6bTBHWOWwIDAQAB
-----END PUBLIC KEY-----`;
	var pubblicData1 = jsencrypt.setLongEncrypt(publiukey, str);
	return pubblicData1;
}

//从客户端到服务端的解密
function getRSADecrypt(str) {
	var publiukey = `-----BEGIN RSA PRIVATE KEY-----
	MIICXAIBAAKBgQCQb5UA73PJ/VIli6zoE2q84EAEzMXZIKROtAwW/7uhNwIeRQRn
	frSN/qUjyNhTETExEUFq4s/qUaNA/V4rDo2gtMtIdjAyMBbsmwd7o3xuRZyngTWk
	G1Ht4oCI5ARosxNZ1N3XFnnMNWqgphnTn8BB6TKAuS1MqMFl7g4HGP3ZZwIDAQAB
	AoGAFlHH2snQlh7D5PK9Q2z3X5HfvV4kwgrImTOQHWBjZDmV5YuQxKrQcpOD0rsk
	rUweJ+ZHZPcBCMVDebWC1HB+ngyjXZPUbqdtZJxrjoehjZES+qkBLg8c0GZcbKfx
	zTKSTmAbYp0rToTU8mKIANar41MYUiWkxi+0hichbIsEnmECQQCinvODPtkmTSLw
	S3LngYLS6iC9qudrPOZ1cqk9WYvQ+Kp32Lio4rytnZdmmbCtQlvZqxYJ4HlQ1jhw
	buyMqbxJAkEA4190omgKqamqrkCaMDT2M17LjfCDfAVVzxzLtKjyT0SJ22ZaoGav
	rB0jFCFcBmjRGwOiwKa8WFa/hdKCxGIILwJAXO1L5fHiELr/fiT/Mj+VzISx1PB2
	cZdJck/lWgNznCSc9QNI8mkuvF9ThvPIPBmxdv8HBvnZGVJwyf74+aB80QJBALQw
	dL00GKcmV6YqekcT4b0KNeSxo3HIxJ0TI/hzHzxkg5/E+LM/TZ7npGISodxOetCe
	y/9C4+ZaMQO7tXgufG8CQH+3VNDKUuCPUXHlSRkaZslAcqGiBKbHNIerdSyYTkrW
	piPDXIaUtmzT3kqOapDuOSBl/GSRbRdTjdX6auuhdo8=
	-----END RSA PRIVATE KEY-----`;
	var openDate = jsencrypt.setDecrypt(publiukey, str);
	return openDate;
}

function getRSALongDecrypt(str) {
	var publiukey = `-----BEGIN RSA PRIVATE KEY-----
	MIICXAIBAAKBgQCQb5UA73PJ/VIli6zoE2q84EAEzMXZIKROtAwW/7uhNwIeRQRn
	frSN/qUjyNhTETExEUFq4s/qUaNA/V4rDo2gtMtIdjAyMBbsmwd7o3xuRZyngTWk
	G1Ht4oCI5ARosxNZ1N3XFnnMNWqgphnTn8BB6TKAuS1MqMFl7g4HGP3ZZwIDAQAB
	AoGAFlHH2snQlh7D5PK9Q2z3X5HfvV4kwgrImTOQHWBjZDmV5YuQxKrQcpOD0rsk
	rUweJ+ZHZPcBCMVDebWC1HB+ngyjXZPUbqdtZJxrjoehjZES+qkBLg8c0GZcbKfx
	zTKSTmAbYp0rToTU8mKIANar41MYUiWkxi+0hichbIsEnmECQQCinvODPtkmTSLw
	S3LngYLS6iC9qudrPOZ1cqk9WYvQ+Kp32Lio4rytnZdmmbCtQlvZqxYJ4HlQ1jhw
	buyMqbxJAkEA4190omgKqamqrkCaMDT2M17LjfCDfAVVzxzLtKjyT0SJ22ZaoGav
	rB0jFCFcBmjRGwOiwKa8WFa/hdKCxGIILwJAXO1L5fHiELr/fiT/Mj+VzISx1PB2
	cZdJck/lWgNznCSc9QNI8mkuvF9ThvPIPBmxdv8HBvnZGVJwyf74+aB80QJBALQw
	dL00GKcmV6YqekcT4b0KNeSxo3HIxJ0TI/hzHzxkg5/E+LM/TZ7npGISodxOetCe
	y/9C4+ZaMQO7tXgufG8CQH+3VNDKUuCPUXHlSRkaZslAcqGiBKbHNIerdSyYTkrW
	piPDXIaUtmzT3kqOapDuOSBl/GSRbRdTjdX6auuhdo8=
	-----END RSA PRIVATE KEY-----`;
	var openDate = jsencrypt.setDecryptArray(publiukey, str);
	return openDate;
}

//验证签名是否正确
function setSignYesorNo(header_sign, data, time) {
	var Ti = "āōêīǐǔǚɑ×÷/∫∮∝∞ξοπρστυφ"
	var sign = md5(data + time + Ti).toUpperCase()
	if (header_sign === sign) {
		return true;
	} else {
		return false;
	}
}

// 查询闲置商品数据
router.post('/selectXZImages', (req, res) => {
	//闲置商品数据请求语句
	let sqlStr = sql.user.selectXz;
	//取出需要提前的页数 闲置 page=1 表示 一页取出6条数据
	let params = req.body;
	//请求头数据
	var header_sign = req.headers.sign
	var header_time = req.headers.timestamp
	//加密 对请求头进行一个判断
	var objdata = params.a
	//先判断请求头的签名是否符合
	if (setSignYesorNo(header_sign, objdata, header_time)) {
		let conn = new DBHelper().getConn();
		var openDate = getRSADecrypt(params.a)
		console.log('数据提取到的是：' + openDate)
		if (openDate === null) {
			res.json({
				'status': 400,
				'msg': '异常操作，请联系客服！',
				'data': ''
			})
		} else {
			var arr = openDate.split('=');
			var page = parseInt(arr[1])
			console.log('数据是：' + page)
			if (arr[0] != 'page' || isNaN(page)) {
				console.log('提交格式有误')
				return res.json({
					'status': 400,
					'msg': '提交格式有误。',
					'data': ''
				})

			}
			var LIMIT1;
			var LIMIT2;
			if (page === 1) {
				LIMIT1 = 0;
				LIMIT2 = 6;
			} else {
				LIMIT2 = 6 * page
				LIMIT1 = LIMIT2 - 6
			}

			conn.query(sqlStr, [LIMIT1, LIMIT2], (err, result) => {
				var text1 = ''
				var text2 = ''
				var data = ''
				if (result.length === 0) {
					res.json({
						'status': 101,
						'msg': '暂时没有更多的数据了。',
						'data': ''
					})
					console.log("und 暂无数据")
				} else {
					result.forEach((item, index, arr) => {
						var r_xz = item.r_xz
						var time = item.time
						var userName = item.userName
						var userImg = item.userImg
						var userWx = item.wechatNum
						var userIphone = item.phoneNum
						var price = item.price
						var detail = item.detail
						var img1 = item.img1
						var img2 = item.img2
						var img3 = item.img3
						var like_value = item.like_value
						var watch_value = item.watch_value
						text1 = text1 + '{"r_xz":"' + r_xz +
							'","time":"' + time +
							'","userName":"' + userName +
							'","userImg":"' + userImg +
							'","userWx":"' + userWx +
							'","userIphone":"' + userIphone +
							'","price":"' + price +
							'","detail":"' + detail +
							'","img1":"' + img1 +
							'","img2":"' + img2 +
							'","img3":"' + img3 +
							'","like_value":"' + like_value +
							'","watch_value":"' + watch_value +
							'"},'
						// //存data1数据
						// if(index%2==0){
						// 	text1 ='{"r_xz":"' + r_xz +
						// 		'","time":"' + time +
						// 		'","userName":"' + userName +
						// 		'","userImg":"' + userImg +
						// 		'","price":"' + price +
						// 		'","detail":"' + detail +
						// 		'","img1":"' + img1 +
						// 		'","img2":"' + img2 +
						// 		'","img3":"' + img3 +
						// 		'","like_value":"' + like_value +
						// 		'","watch_value":"' + watch_value +
						// 		'"}'
						// }else if(index%2==1){
						// 	text2 ='{"r_xz":"' + r_xz +
						// 		'","time":"' + time +
						// 		'","userName":"' + userName +
						// 		'","userImg":"' + userImg +
						// 		'","price":"' + price +
						// 		'","detail":"' + detail +
						// 		'","img1":"' + img1 +
						// 		'","img2":"' + img2 +
						// 		'","img3":"' + img3 +
						// 		'","like_value":"' + like_value +
						// 		'","watch_value":"' + watch_value +
						// 		'"}'
						// 		data =data+'{"data1":"'+text1+'","data2":"'+text2+'"},'
						// 		text1=''
						// 		text2=''
						// }


					})
					data = "[" + text1.substr(0, text1.length - 1) + "]"
					console.log("组合后的数据：" + data)
					if (data != '[]') {
						var Eccode = getRSAcode(data)
						//console.log("加密数据:"+Eccode)
						res.json({
							'status': 200,
							'msg': '',
							'data': Eccode
						})
					} else {
						res.json({
							'status': 400,
							'msg': '异常操作，请联系客服！',
							'data': ''
						})
					}
				}
			});
		}
		conn.end();
	} else {
		res.json({
			'status': 410,
			'msg': '异常操作，请联系客服！',
			'data': ''
		})
	}
});


// 查询首页Api图片
router.post('/selectImages', (req, res) => {
	//sql 语句
	let sqlStr = sql.user.select;
	//提交的数据
	let params = req.body;
	//请求头数据
	var header_sign = req.headers.sign
	var header_time = req.headers.timestamp
	//加密 对请求头进行一个判断
	var objdata = params.a
	//先判断请求头的签名是否符合
	if (setSignYesorNo(header_sign, objdata, header_time)) {
		let conn = new DBHelper().getConn();
		var openDate = getRSADecrypt(params.a)
		//console.log("解密" + openDate)
		//console.log("请求头时间"+header_time)
		//console.log("请求头"+header_sign)
		if (openDate === null) {
			res.json({
				'status': 400,
				'msg': '异常操作，请联系客服！',
				'data': ''
			})
		} else {
			conn.query(sqlStr, [openDate], (err, result) => {
				var text = ''
				result.forEach((item, index, arr) => {
					var ImgUrl = item.ImgUrl
					var ImgName = item.ImgName
					var type = item.type
					text = text + '{"ImgUrl":"' + ImgUrl + '","ImgName":"' + ImgName +
						'","type":"' +
						type + '"},'
				})
				text = "[" + text.substr(0, text.length - 1) + "]"
				console.log(text)
				if (text != '[]') {
					var Eccode = getRSAcode(text)
					//console.log("加密数据:"+Eccode)
					res.json({
						'status': 200,
						'msg': '',
						'data': Eccode
					})
				} else {
					res.json({
						'status': 400,
						'msg': '异常操作，请联系客服！',
						'data': ''
					})
				}
			});
		}
		conn.end();
	} else {
		res.json({
			'status': 410,
			'msg': '异常操作，请联系客服！',
			'data': ''
		})
	}


});

//用户登陆
router.post('/v1/users/wxlogin', (req, res) => {
	let sqlSelect = sql.user.selectUser; //用户表查询
	let sqlInsertTo = sql.user.NewInsertTouser; //用户添加
	let params = req.body;
	var wxcode = params.code;
	//请求头数据
	var header_sign = req.headers.sign;
	var header_time = req.headers.timestamp;
	//加密 对请求头进行一个判断
	//先判断请求头的签名是否符合
	if (setSignYesorNo(header_sign, wxcode, header_time)) {
		console.log("验签成功！！")
		const userinfo = JSON.parse(params.rawData)
		var username = userinfo.nickName
		var userImg = userinfo.avatarUrl
		console.log(username, userImg)
		axios.get(
				' https://api.weixin.qq.com/sns/jscode2session?appid=wx222dc4a9ee4caf0d&secret=01f6f2462b9ad0d865869ceb1884cef5&js_code=' +
				wxcode + '&grant_type=authorization_code')
			.then(response => {
				var openid = response.data.openid;
				console.log(openid)
				let conn = new DBHelper().getConn();
				conn.query(sqlSelect, [openid], (err, result) => {
					//如果返回的长度等于0说明 数据库内没有该用户 应初始化该用户数据
					if (result.length === 0) {
						let conn1 = new DBHelper().getConn();
						conn1.query(sqlInsertTo, [openid, 'null', username, userImg, '新用户', 'null',
							'null',
							'null'
						], (err, result) => {
							console.log("添加数据后的返回值" + result)
							const data = {
								openid: openid,
								userid: 'null',
								userName: username,
								userImg: userImg,
								UserType: '新用户',
								phoneNum: 'null',
								photo: 'null',
								wechatNum: 'null',
							}
							res.json({
								'status': 200,
								'msg': '获取成功！',
								'data': data,
							})
						});
						conn1.end();
					} else {
						res.json({
							'status': 200,
							'msg': '获取成功！',
							'data': result,
						})
					}
				});
				conn.end();
			})
			.catch(error => {
				console.log(error);
				res.json({
					'status': 404,
					'msg': '获取用户数据失败',
					'data': ''
				})
			});
	}

});

//跑腿数据查询
router.post('/Ptselect_address', (req, res) => {
	//sql 语句
	let sqlStr = sql.user.Ptselect_address;
	//提交的数据
	let params = req.body;
	//请求头数据
	var header_sign = req.headers.sign
	var header_time = req.headers.timestamp
	//加密 对请求头进行一个判断
	var objdata = params.a
	//先判断请求头的签名是否符合
	if (setSignYesorNo(header_sign, objdata, header_time)) {
		let conn = new DBHelper().getConn();
		var openDate = getRSADecrypt(params.a)
		//console.log("解密" + openDate)
		//console.log("请求头时间"+header_time)
		//console.log("请求头"+header_sign)
		if (openDate === null) {
			res.json({
				'status': 400,
				'msg': '异常操作，请联系客服！',
				'data': ''
			})
		} else {
			conn.query(sqlStr, [openDate], (err, result) => {
				var text = JSON.stringify(result)
				console.log("跑腿数据查询:" + text)
				if (text != '[]' && text != undefined) {
					var Eccode = getRSAcode(text)
					//console.log("加密数据:"+Eccode)
					res.json({
						'status': 200,
						'msg': '',
						'data': Eccode
					})
				} else {
					res.json({
						'status': 400,
						'msg': '异常操作，请联系客服！',
						'data': ''
					})
				}
			});
		}
		conn.end();
	} else {
		res.json({
			'status': 410,
			'msg': '异常操作，请联系客服！',
			'data': ''
		})
	}
});
//跑腿编辑地址-修改
router.post('/Ptupdate_address', (req, res) => {
	//sql 语句
	let sqlStr = sql.user.Ptupdate_address;
	//提交的数据
	let params = req.body;
	//请求头数据
	var header_sign = req.headers.sign
	var header_time = req.headers.timestamp
	//加密 对请求头进行一个判断
	var objdata = params.a
	//先判断请求头的签名是否符合
	if (setSignYesorNo(header_sign, objdata, header_time)) {
		let conn = new DBHelper().getConn();
		var openDate = getRSALongDecrypt(params.a)
		const objData = JSON.parse(openDate)
		console.log(openDate)
		//console.log("解密" + objData.name)
		//console.log("请求头时间"+header_time)
		//console.log("请求头"+header_sign)
		if (openDate === null) {
			res.json({
				'status': 400,
				'msg': '异常操作，请联系客服！',
				'data': ''
			})
		} else {
			conn.query(sqlStr, [
				objData.name,
				objData.phone,
				objData.region,
				objData.Detailed_address,
				objData.areavalue,
				objData.defaulta,
				objData.No,
				objData.openid
			], (err, result) => {
				var text = JSON.stringify(result)
				console.log("跑腿编辑地址-修改:" + text)
				if (text != '[]' && text != undefined) {
					//var Eccode = getRSAcode(text)
					//console.log("加密数据:"+Eccode)
					res.json({
						'status': 200,
						'msg': 'ok',
						'data': ''
					})
				} else {
					res.json({
						'status': 400,
						'msg': '异常操作，请联系客服！',
						'data': ''
					})
				}
			});
		}
		conn.end();
	} else {
		res.json({
			'status': 410,
			'msg': '异常操作，请联系客服！',
			'data': ''
		})
	}
});

//跑腿编辑地址-删除
router.post('/PtDelete_address', (req, res) => {
	//sql 语句
	let sqlStr = sql.user.PtDelete_address;
	//提交的数据
	let params = req.body;
	//请求头数据
	var header_sign = req.headers.sign
	var header_time = req.headers.timestamp
	//加密 对请求头进行一个判断
	var objdata = params.a
	//先判断请求头的签名是否符合
	if (setSignYesorNo(header_sign, objdata, header_time)) {
		let conn = new DBHelper().getConn();
		var openDate = getRSADecrypt(params.a)
		const objData = JSON.parse(openDate)
		console.log(openDate)
		//console.log("解密" + objData.name)
		//console.log("请求头时间"+header_time)
		//console.log("请求头"+header_sign)
		if (openDate === null) {
			res.json({
				'status': 400,
				'msg': '异常操作，请联系客服！',
				'data': ''
			})
		} else {
			conn.query(sqlStr, [
				objData.No,
				objData.openid
			], (err, result) => {
				var text = JSON.stringify(result)
				console.log("跑腿编辑地址-删除:" + text)
				if (text != '[]' && text != undefined) {
					//var Eccode = getRSAcode(text)
					//console.log("加密数据:"+Eccode)
					res.json({
						'status': 200,
						'msg': 'ok',
						'data': ''
					})
				} else {
					res.json({
						'status': 400,
						'msg': '异常操作，请联系客服！',
						'data': ''
					})
				}
			});
		}
		conn.end();
	} else {
		res.json({
			'status': 410,
			'msg': '异常操作，请联系客服！',
			'data': ''
		})
	}
});
//跑腿地址-添加
router.post('/Ptinsert_address', (req, res) => {
	//sql 语句
	let sqlStr = sql.user.Ptinsert_address;
	//sql 语句-跑腿修改默认值-添加过程协调修改
	var sqlStr_update = sql.user.Ptupdate_defalut;
	//提交的数据
	let params = req.body;
	//请求头数据
	var header_sign = req.headers.sign
	var header_time = req.headers.timestamp
	//加密 对请求头进行一个判断
	var objdata = params.a
	//先判断请求头的签名是否符合
	if (setSignYesorNo(header_sign, objdata, header_time)) {
		let conn = new DBHelper().getConn();
		console.log(params.a)
		var openDate = getRSALongDecrypt(params.a)
		const objData = JSON.parse(openDate)
		//console.log(openDate)
		//console.log("解密" + objData.name)
		//console.log("请求头时间"+header_time)
		//console.log("请求头"+header_sign)
		if (openDate === null) {
			res.json({
				'status': 400,
				'msg': '异常操作，请联系客服！',
				'data': ''
			})
		} else {
			if (objData.defaulta === 1) {
				conn.query(sqlStr_update, [
					objData.openid,
				], (err, result) => {

				})
			}
			conn.query(sqlStr, [
				objData.openid,
				objData.name,
				objData.phone,
				objData.region,
				objData.Detailed_address,
				objData.areavalue,
				objData.defaulta,
			], (err, result) => {
				var text = JSON.stringify(result)
				console.log("跑腿地址-添加:" + text)
				if (text != '[]' && text != undefined) {
					//var Eccode = getRSAcode(text)
					//console.log("加密数据:"+Eccode)
					res.json({
						'status': 200,
						'msg': 'ok',
						'data': ''
					})
				} else {
					res.json({
						'status': 400,
						'msg': '异常操作，请联系客服！',
						'data': ''
					})
				}
			});
		}
		conn.end();
	} else {
		res.json({
			'status': 410,
			'msg': '异常操作，请联系客服！',
			'data': ''
		})
	}
});

//跑腿帮我取(买)-下单
router.post('/Ptinsert_order', (req, res) => {
	//sql 语句
	let sqlStr = sql.user.Ptinsert_order;
	//sql 语句-跑腿帮我取(买)-发布表添加
	var Ptinsert_releace_order = sql.user.Ptinsert_releace_order;
	//提交的数据
	let params = req.body;
	//请求头数据
	var header_sign = req.headers.sign
	var header_time = req.headers.timestamp
	//加密 对请求头进行一个判断
	var objdata = params.a
	//先判断请求头的签名是否符合
	if (setSignYesorNo(header_sign, objdata, header_time)) {
		let conn = new DBHelper().getConn();
		console.log(params.a)
		var openDate = getRSALongDecrypt(params.a)
		const objData = JSON.parse(openDate)
		console.log(openDate)
		console.log("解密" + objData.No)
		//console.log("请求头时间"+header_time)
		//console.log("请求头"+header_sign)
		if (openDate === null) {
			res.json({
				'status': 400,
				'msg': '异常操作，请联系客服！',
				'data': ''
			})
		} else {
			var img = ''
			const objImg = {}
			if (objData.img.length > 0) {
				for (var i = 0; i < objData.img.length; i++) {
					objImg[i] = objData.img[i]
				}
				img = JSON.stringify(objImg)
			}
			console.log("图片增加：" + img)
			
			conn.query(sqlStr, [
				objData.No,
				objData.address_No,
				objData.time,
				objData.by_Name,
				objData.price,
				objData.range,
				objData.phoneNum,
				objData.detail,
				img,
			], (err, result) => {
				var text = JSON.stringify(result)
				console.log("跑腿订单-添加:" + text)
				if (text != '[]' && text != undefined) {
					//var Eccode = getRSAcode(text)
					//console.log("加密数据:"+Eccode)
					let conn1 = new DBHelper().getConn();
					var releaceNo = md5((new Date()).valueOf())
					//console.log(releaceNo.substring(1, 16))
					conn1.query(Ptinsert_releace_order, [
						releaceNo.substring(1, 16),
						objData.openid,
						objData.No,
						objData.PlaceOrderTime,
					], (err, result) => {
						res.json({
							'status': 200,
							'msg': 'ok',
							'data': ''
						})
					});
					conn1.end();
				} else {
					res.json({
						'status': 400,
						'msg': '异常操作，请联系客服！' + err,
						'data': ''
					})
				}
			});
		}
		conn.end();
	} else {
		res.json({
			'status': 410,
			'msg': '异常操作，请联系客服！',
			'data': ''
		})
	}
});

//发布-失物招领
router.post('/Fbinsert_xw_add', (req, res) => {
	//sql 语句
	let sqlStr = sql.user.Fbinsert_xw_add;
	//sql 语句-跑腿帮我取(买)-发布表添加
	var Fbinsert_releace_xw_add = sql.user.Fbinsert_releace_xw_add;
	//提交的数据
	let params = req.body;
	//请求头数据
	var header_sign = req.headers.sign
	var header_time = req.headers.timestamp
	//加密 对请求头进行一个判断
	var objdata = params.a
	//先判断请求头的签名是否符合
	if (setSignYesorNo(header_sign, objdata, header_time)) {
		let conn = new DBHelper().getConn();
		console.log(params.a)
		var openDate = getRSALongDecrypt(params.a)
		const objData = JSON.parse(openDate)
		console.log(openDate)
		console.log("解密" + objData.No)
		//console.log("请求头时间"+header_time)
		//console.log("请求头"+header_sign)
		if (openDate === null) {
			res.json({
				'status': 400,
				'msg': '异常操作，请联系客服！',
				'data': ''
			})
		} else {
			var img = ''
			const objImg = {}
			if (objData.SwImage.length > 0) {
				for (var i = 0; i < objData.SwImage.length; i++) {
					objImg[i] = objData.SwImage[i]
				}
				img = JSON.stringify(objImg)
			}
			console.log("图片增加：" + img)
			
			conn.query(sqlStr, [
				objData.No,
				objData.Swname,
				objData.Swphone,
				objData.Swgoods,
				objData.SwText,
				objData.Swtype,
				img,
			], (err, result) => {
				var text = JSON.stringify(result)
				console.log("失物招领-添加:" + text)
				if (text != '[]' && text != undefined) {
					//var Eccode = getRSAcode(text)
					//console.log("加密数据:"+Eccode)
					let conn1 = new DBHelper().getConn();
					var releaceNo = md5((new Date()).valueOf())
					//console.log(releaceNo.substring(1, 16))
					conn1.query(Fbinsert_releace_xw_add, [
						releaceNo.substring(1, 16),
						objData.openid,
						objData.No,
						objData.PlaceOrderTime,
					], (err, result) => {
						res.json({
							'status': 200,
							'msg': 'ok',
							'data': ''
						})
					});
					conn1.end();
				} else {
					res.json({
						'status': 400,
						'msg': '异常操作，请联系客服！' + err,
						'data': ''
					})
				}
			});
		}
		conn.end();
	} else {
		res.json({
			'status': 410,
			'msg': '异常操作，请联系客服！',
			'data': ''
		})
	}
});

//发布-二手闲置
router.post('/Fbinsert_xz_add', (req, res) => {
	//sql 语句
	let sqlStr = sql.user.Fbinsert_xz_add;
	//sql 语句-跑腿帮我取(买)-发布表添加
	var Fbinsert_releace_xz_add = sql.user.Fbinsert_releace_xz_add;
	//提交的数据
	let params = req.body;
	//请求头数据
	var header_sign = req.headers.sign
	var header_time = req.headers.timestamp
	//加密 对请求头进行一个判断
	var objdata = params.a
	//先判断请求头的签名是否符合
	if (setSignYesorNo(header_sign, objdata, header_time)) {
		let conn = new DBHelper().getConn();
		console.log(params.a)
		var openDate = getRSALongDecrypt(params.a)
		const objData = JSON.parse(openDate)
		console.log(openDate)
		console.log("解密" + objData.No)
		//console.log("请求头时间"+header_time)
		//console.log("请求头"+header_sign)
		if (openDate === null) {
			res.json({
				'status': 400,
				'msg': '异常操作，请联系客服！',
				'data': ''
			})
		} else {
			var img = ''
			const objImg = {}
			if (objData.XzImage.length > 0) {
				for (var i = 0; i < objData.XzImage.length; i++) {
					objImg[i] = objData.XzImage[i]
				}
				img = JSON.stringify(objImg)
			}
			console.log("图片增加：" + img)
			
			conn.query(sqlStr, [
				objData.No,
				objData.Xzprice,
				'0',
				'0',
				objData.Xzphone,
				objData.Xzwx,
				objData.XzText,
				objData.Xztype,
				img,
			], (err, result) => {
				var text = JSON.stringify(result)
				console.log("二手闲置-添加:" + text)
				if (text != '[]' && text != undefined) {
					//var Eccode = getRSAcode(text)
					//console.log("加密数据:"+Eccode)
					let conn1 = new DBHelper().getConn();
					var releaceNo = md5((new Date()).valueOf())
					//console.log(releaceNo.substring(1, 16))
					conn1.query(Fbinsert_releace_xz_add, [
						releaceNo.substring(1, 16),
						objData.openid,
						objData.No,
						objData.PlaceOrderTime,
					], (err, result) => {
						res.json({
							'status': 200,
							'msg': 'ok',
							'data': ''
						})
					});
					conn1.end();
				} else {
					res.json({
						'status': 400,
						'msg': '异常操作，请联系客服！' + err,
						'data': ''
					})
				}
			});
		}
		conn.end();
	} else {
		res.json({
			'status': 410,
			'msg': '异常操作，请联系客服！',
			'data': ''
		})
	}
});

//发布-兼职发布
router.post('/Fbinsert_jz_add', (req, res) => {
	//sql 语句
	let sqlStr =  sql.user.Fbinsert_jz_add;
	//sql 语句-跑腿帮我取(买)-发布表添加
	var Fbinsert_releace_jz_add = sql.user.Fbinsert_releace_jz_add;
	//提交的数据
	let params = req.body;
	//请求头数据
	var header_sign = req.headers.sign
	var header_time = req.headers.timestamp
	//加密 对请求头进行一个判断
	var objdata = params.a
	//先判断请求头的签名是否符合
	if (setSignYesorNo(header_sign, objdata, header_time)) {
		let conn = new DBHelper().getConn();
		console.log(params.a)
		var openDate = getRSALongDecrypt(params.a)
		const objData = JSON.parse(openDate)
		console.log(openDate)
		console.log("解密" + objData.No)
		//console.log("请求头时间"+header_time)
		//console.log("请求头"+header_sign)
		if (openDate === null) {
			res.json({
				'status': 400,
				'msg': '异常操作，请联系客服！',
				'data': ''
			})
		} else {		
			conn.query(sqlStr, [
				objData.No,
				objData.Jztitle,
				objData.Jzdlie,
				objData.Jztime,
				objData.Jzdata,
				objData.Jzmoney,
				objData.work_detail,
				objData.work_require,
				objData.JzCompany,
				objData.Jzphone,
				objData.Jzdemand,
				objData.Jztype,
			], (err, result) => {
				var text = JSON.stringify(result)
				console.log("兼职发布-添加:" + text)
				if (text != '[]' && text != undefined) {
					//var Eccode = getRSAcode(text)
					//console.log("加密数据:"+Eccode)
					let conn1 = new DBHelper().getConn();
					var releaceNo = md5((new Date()).valueOf())
					//console.log(releaceNo.substring(1, 16))
					conn1.query(Fbinsert_releace_jz_add, [
						releaceNo.substring(1, 16),
						objData.openid,
						objData.No,
						objData.PlaceOrderTime,
					], (err, result) => {
						res.json({
							'status': 200,
							'msg': 'ok',
							'data': ''
						})
					});
					conn1.end();
				} else {
					res.json({
						'status': 400,
						'msg': '异常操作，请联系客服！' + err,
						'data': ''
					})
				}
			});
		}
		conn.end();
	} else {
		res.json({
			'status': 410,
			'msg': '异常操作，请联系客服！',
			'data': ''
		})
	}
});



router.get('/getlist', (req, res) => {
	var sql = sql.user.select
	var parms = req.query
	console.log(parms)
	conn.query(sql, function(err, result) {
		if (err) {
			console.log(err)
		}
		if (result) {
			console.log(result)
			res.send(result)
		}
	})
});



module.exports = router;
