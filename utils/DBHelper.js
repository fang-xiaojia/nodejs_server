// 数据库连接助手
const mysql = require('mysql');

class DBHelper{
    // 获取数据库连接
    getConn(){
        let conn = mysql.createConnection({
            // 数据库连接配置
            host:'124.222.72.174',
            port:'3306', 
            user:'root',
            password:'aa123456',
            database:'zc_project'  // 数据库名
        });
        conn.connect(err=>{
            console.log(err);
        });
        return conn;
    }
}

module.exports = DBHelper;
