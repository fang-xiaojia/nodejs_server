# nodejs_server

#### 介绍
微信小程序后端接口对接mysl数据库

MD5 组合验签
        RSA 双向加解密



> #### 2022年4月24日晚 设计微信用户登陆接口
>
> ####  请求地址： Post : /api/project/v1/users/wxlogin
>
> #### 请求参数：（以下参数均为调用uni.login接口返回的数据需传到后端进行校验转换openid ）
>
> | 字段          | 说明                     | 类型   | 备注 | 是否必填 |
> | ------------- | ------------------------ | ------ | ---- | -------- |
> | code          | 登陆code需到后端换openid | String |      | 是       |
> | encryptedData | 加密数据                 | String |      | 否       |
> | iv            | 偏移值                   | String |      | 否       |
> | rawData       | 个人信息                 | String |      | 是       |
> | signature     | 签名                     | String |      | 否       |
>
> #### 返回参数
>
> | 字段   | 说明                              | 类型   | 备注 | 是否必填 |
> | ------ | --------------------------------- | ------ | ---- | -------- |
> | status | 200:登陆成功 404:获取用户数据失败 | Number |      | 是       |
> | msg    | 获取成功！/登陆成功！             | String |      | 是       |
> | data   | 用户信息数据                      | Object |      | 是       |
>
> data:
>
> | 字段      | 说明                             | 类型   | 备注 | 是否必填 |
> | --------- | -------------------------------- | ------ | ---- | -------- |
> | openid    | 微信唯一标识                     | String |      | 是       |
> | userid    | 学号/工号                        | String |      | 是       |
> | userName  | 用户名                           | String |      | 是       |
> | userImg   | 用户微信头像                     | String |      | 是       |
> | UserType  | 用户认证类型（学生、跑腿...）    | String |      | 是       |
> | phoneNum  | 手机号                           | String |      | 是       |
> | wechatNum | 微信号                           | String |      | 是       |
> | photo     | 认证图片信息（身份证、学生证等） | Object |      | 是       |

### 跑腿-用户地址数据查询

- #### 请求地址：Post   /api/project/Ptselect_address

- #### 请求参数：{"a":加密（openid）}

- #### 返回参数

| 字段         | 说明                          | 类型   | 备注 | 是否必填 |
| ------------ | ----------------------------- | ------ | ---- | -------- |
| No           | 编号                          | int    |      | 是       |
| openid       | 用户唯一标识                  | String |      | 是       |
| name         | 用户名称                      | String |      | 是       |
| phoneNum     | 手机号                        | String |      | 是       |
| place        | 所在地区                      | String |      | 是       |
| address      | 详细地址                      | String |      | 是       |
| areavalue    | 所在地区区号                  | String |      | 是       |
| default_type | 默认类型值 0 不默认 或 1 默认 | int    |      | 是       |

> 解密数据[{"No":3,"openid":"owYEL46qJlGSFtTCxE0lGsaDLoqA","name":"方晓佳aa","phoneNum":"13559453228","place":"福建省福州市晋安区","address":"福州大学至诚学院中区2号楼405","areavalue":"350111","default_type":0},{"No":5,"openid":"owYEL46qJlGSFtTCxE0lGsaDLoqA","name":"方晓佳","phoneNum":"13559453225","place":"福建省福州市鼓楼区","address":"福州大学至诚学院","areavalue":"350102","default_type":0},{"No":9,"openid":"owYEL46qJlGSFtTCxE0lGsaDLoqA","name":"方晓佳","phoneNum":"13559453225","place":"天津市天津市河东区","address":"福州大学至诚学院中区2号楼405","areavalue":"120102","default_type":0}]

### 跑腿-编辑地址-修改

- #### 请求地址：Post   /api/project/Ptupdate_address

- #### 请求参数：{"a":加密（json数据）}

| 字段         | 说明                          | 类型   | 备注 | 是否必填 |
| ------------ | ----------------------------- | ------ | ---- | -------- |
| No           | 编号                          | int    |      | 是       |
| openid       | 用户唯一标识                  | String |      | 是       |
| name         | 用户名称                      | String |      | 是       |
| phoneNum     | 手机号                        | String |      | 是       |
| place        | 所在地区                      | String |      | 是       |
| address      | 详细地址                      | String |      | 是       |
| areavalue    | 所在地区区号                  | String |      | 是       |
| default_type | 默认类型值 0 不默认 或 1 默认 | int    |      | 是       |

- #### 返回参数

```json
成功返回：{'status': 200,'msg': 'ok','data': ''}
失败返回：{'status': 400,'msg': '异常操作，请联系客服！','data': ''}
```

### 跑腿-编辑地址-删除

- #### 请求地址：Post   /api/project/PtDelete_address

- #### 请求参数：{"a":加密（json数据）}

| 字段   | 说明         | 类型   | 备注 | 是否必填 |
| ------ | ------------ | ------ | ---- | -------- |
| No     | 编号         | int    |      | 是       |
| openid | 用户唯一标识 | String |      | 是       |

- #### 返回参数

```json
成功返回：{'status': 200,'msg': 'ok','data': ''}
失败返回：{'status': 400,'msg': '异常操作，请联系客服！','data': ''}
```

### 跑腿-地址-添加

- #### 请求地址：Post   /api/project/Ptinsert_address

- #### 请求参数：{"a":加密（json数据）}

| 字段         | 说明                          | 类型   | 备注 | 是否必填 |
| ------------ | ----------------------------- | ------ | ---- | -------- |
| No           | 编号                          | int    |      | 是       |
| openid       | 用户唯一标识                  | String |      | 是       |
| name         | 用户名称                      | String |      | 是       |
| phoneNum     | 手机号                        | String |      | 是       |
| place        | 所在地区                      | String |      | 是       |
| address      | 详细地址                      | String |      | 是       |
| areavalue    | 所在地区区号                  | String |      | 是       |
| default_type | 默认类型值 0 不默认 或 1 默认 | int    |      | 是       |

- #### 返回参数

```json
成功返回：{'status': 200,'msg': 'ok','data': ''}
失败返回：{'status': 400,'msg': '异常操作，请联系客服！','data': ''}
```

